# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/9/21 09:37
# File      : stest_get_userinfo.py
# explain   : 文件说明
import pytest

from Common.AssetCase import TestCase
from Api.GetUserInfo import GetUserInfos
from Common.HelpUser import UserActionToken
from Common.ReadYaml import get_yml_data
from Common.Log_utils import logger


class TestGetUserInfo(TestCase):

    @classmethod
    def setup_class(cls):
        cls.action = GetUserInfos()

    @pytest.mark.parametrize('case_data', get_yml_data('Action', 'userinfo', 'userinfo'))
    def test_getuser_info(self, case_data):
        self.assertJson(case_data.get('verify'), self.action.get_user_info(case_data['palayod']).json())


if __name__ == '__main__':
    pytest.main(['-s', '-v', 'stest_get_userinfo.py'])
