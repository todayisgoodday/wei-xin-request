# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/9/20 17:20
# File      : test_get_token.py
# explain   : 文件说明

import pytest
from Common.ReadYaml import get_yml_data
from Common.AssetCase import TestCase
from Api.GetToken import GetToken


class TestGetToken(TestCase):

    @classmethod
    def setup_class(cls):
        cls.action = GetToken()

    @pytest.mark.parametrize('case_data', get_yml_data('Action', 'token', 'get_token'))
    def test_login(self, case_data):
        """
        登录成功
        :param case_data:
        :return:
        """
        self.assertJson(case_data.get('verify'),
                        self.action.get_token(case_data['palayod']).json()), '登录失败，测试用例报错......'


if __name__ == '__main__':
    pytest.main(['-s', '-v', 'test_get_token.py'])
