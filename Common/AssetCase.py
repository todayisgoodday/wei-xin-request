# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/9/19 14:24
# File      : AssetCase.py
# explain   : 文件说明


import decimal
from Common.Diff import AssertInfo, diff_json
from Common.Request import HttpRequest, ResponseResult
from Common.utils_jmespath import jmespath


class TestCase(HttpRequest):

    def assertStatsCodes(self, status_code, msg=None):
        """
        Asserts the HTTP status code
        """
        assert ResponseResult.status_code == status_code, msg

    def assertJson(self, assert_json, response=None):
        """
        Assert JSON data
        """
        if response is None:
            response = ResponseResult.response
        AssertInfo.data = []
        res = diff_json(response, assert_json)

        if AssertInfo.data:
            assert False, AssertInfo.data

    def assertPath(self, path, value, response=None):
        """
        Assert path data
        doc: https://jmespath.org/
        """
        if response is None:
            response = ResponseResult.response
        # 化为同类型比较
        search_value = jmespath(response, path)
        if type(value) == decimal.Decimal:
            search_value = decimal.Decimal(str(search_value))
        if search_value:
            assert search_value == value, f"{path}对应的值:{search_value}与预期:{value}不符"
        else:
            assert search_value == value, f"{path} 路径不存在"
