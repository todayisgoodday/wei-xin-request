# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/8/15 11:03
# File      : Log_utils.py
# explain   : 文件说明

import os
from nb_log import LogManager

root_dir = os.path.dirname(os.path.abspath(os.path.dirname(__file__)))
log_path = os.path.join(root_dir + '/../' + 'Outputs/Logs')


class LogsUtils:

    def logger(self):
        logger = LogManager('Api_Test').get_logger_and_add_handlers(
            is_add_stream_handler=True,
            log_filename='Auto.log'
        )
        return logger


logger = LogsUtils().logger()
