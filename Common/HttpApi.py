# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/9/7 16:06
# File      : HttpApi.py
# explain   : 文件说明
import json

from Common.Request import HttpRequest


class Api(HttpRequest):
    """
    生产、测试环境分离
    """

    def __init__(self, host):
        self.host = host
        self.proto = 'https'

    def request(self, method, url, payload=None, headers=None, **kwargs):
        """
        调用request自动分配请求方法，集成
        :param method:
        :param url:
        :param payload:
        :param headers:
        :param kwargs:
        :return:
        """
        domain = f"{self.proto}://{self.host}/{url}"

        if method == 'get':
            resp = self.get(domain, params=payload, headers=headers, **kwargs)
        else:
            resp = self.post(domain, json=payload, headers=headers, **kwargs)
        assert resp.status_code == 200, url + "请求异常"
        return resp


if __name__ == '__main__':
    pass
