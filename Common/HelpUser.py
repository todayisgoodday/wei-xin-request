# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/9/20 17:31
# File      : HelpUser.py
# explain   : 文件说明

from time import time
from Common.ReadYaml import get_yml_data, revise_yml_data
from Common.HttpApi import Api
from Common.Log_utils import logger


class UserActionToken():
    def __init__(self):
        self.api = Api(host='api.weixin.qq.com')
        self.token = get_yml_data('Action', 'setup', 'token')[0]['palayod']['access_token']
        self.current_time = get_yml_data('Action', 'setup', 'token')[0]['palayod']['current_time']

    def get_token(self):
        if self.token and (time() - int(self.current_time)) < 7200:
            logger.info('当前token未超过72小时')
            return self.token
        else:
            logger.info("超过72小时了,重新获取token")
            url = 'cgi-bin/token'
            result = self.api.request('get', url,
                                      payload=get_yml_data('Action', 'token', 'get_token')[0]['palayod']).json()
            revise_yml_data('Action', 'setup', 'token', 'palayod', int(time()), 'current_time')
            revise_yml_data('Action', 'setup', 'token', 'palayod', result['access_token'], 'access_token')
            return self.token


if __name__ == '__main__':
    print(UserActionToken().get_token())
