# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/9/7 15:24
# File      : Request.py
# explain   : 文件说明


import requests
from Common.Log_utils import logger


def request(func):
    """
    新增装饰器，调用方法都会进入装饰器中操作
    :param func:
    :return:
    """

    def wrapper(*args, **kwargs):
        func_name = func.__name__
        logger.info("-------------- Request -----------------[🚀]")
        result = func(*args, **kwargs)
        logger.info("-------------- Response ----------------[🛬️]")
        return result

    return wrapper


class ResponseResult:
    status_code = 200
    response = None


class HttpRequest(object):
    @request
    def get(self, url, params=None, **kwargs):
        # if (Seldom.base_url is not None) and ("http" not in url):
        #     url = Seldom.base_url + url
        return requests.get(url, params=params, **kwargs)

    @request
    def post(self, url, data=None, json=None, **kwargs):
        # if (Seldom.base_url is not None) and ("http" not in url):
        #     url = Seldom.base_url + url
        return requests.post(url, data=data, json=json, **kwargs)

    @request
    def put(self, url, data=None, **kwargs):
        # if (Seldom.base_url is not None) and ("http" not in url):
        #     url = Seldom.base_url + url
        return requests.put(url, data=data, **kwargs)

    @request
    def delete(self, url, **kwargs):
        # if (Seldom.base_url is not None) and ("http" not in url):
        #     url = Seldom.base_url + url
        return requests.delete(url, **kwargs)

    @property
    def response(self) -> dict:
        """
        Returns the result of the response
        :return: response
        """
        return ResponseResult.response

    @property
    def status_code(self) -> int:
        """
        Returns the result of the status code
        :return: status_code
        """
        return ResponseResult.status_code
