# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/9/20 17:46
# File      : GetUserInfo.py
# explain   : 文件说明

import requests
from Common.HttpApi import Api
from Common.HelpUser import UserActionToken
from Common.ReadYaml import revise_yml_data, get_yml_data


class GetUserInfos(Api):

    def __init__(self):
        super(GetUserInfos, self).__init__(host='api.weixin.qq.com')
        self.headers = {"Content-Type": "application/json;"}
        self.token = UserActionToken()

    def get_user_info(self, palayod):
        """
        获取token
        :param palayod:
        :return:
        """
        url = 'cgi-bin/user/get'
        revise_yml_data('Action', 'userinfo', 'userinfo', 'palayod', self.token.get_token(), 'access_token')
        return self.request('get', url, palayod, self.headers)


if __name__ == '__main__':
    # print(get_yml_data('Action', 'userinfo','userinfo')[0]['palayod'])
    result = GetUserInfos().get_user_info(get_yml_data('Action', 'userinfo','userinfo')[0]['palayod'])
    print(result.json())
