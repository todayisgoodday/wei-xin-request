# encoding: utf-8
# Author    : limusen
# Datetime  : 2023/9/20 16:56
# File      : GetToken.py
# explain   : 文件说明

import requests
from Common.HttpApi import Api


class GetToken(Api):

    def __init__(self):
        super(GetToken, self).__init__(host='api.weixin.qq.com')

    def get_token(self, palayod):
        """
        获取token
        :param palayod:
        :return:
        """
        url = 'cgi-bin/token'
        return self.request('get', url, palayod)


if __name__ == '__main__':
    palayod = {
        "grant_type": "client_credential",
        "appid": "wxb637f897f0bf1f0d",
        "secret": "501123d2d367b109a5cb9a9011d0f084"
    }
    result = GetToken().get_token(palayod)
    print(result.json())
