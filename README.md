# WeXinRequest

#### 介绍

基于微信公众号开发的接口自动化框架，融合了新的思路

#### 软件架构

pytest + allure + nb_log + jenkins + 钉钉机器人

#### 安装教程

开箱即用

#### 使用说明

1. 修改Data目录下的配置
2. 修改sink/source端的连接器名称即可
3. 如果在不同域名下，则需要修改urls中的域名信息
4. 需要执行测试用例的时候可以采用命令行方式执行
pytest Sample/test_login.py --settings=Config.test-198

   
#### case执行命令：

执行所有：

- window：py.test --alluredir=./test_report testcase/ --settings=config.cht-test-1
- Mac：pytest --alluredir=./test_report testcase/ --settings=config.cht-test-1
- 列出最慢top10的case：pytest --alluredir=./test_report testcase/ --settings=config.cht-test-1 --durations=10
- 报告展示执行过程中的变量信息：pytest testcase -l --settings=config.cht-test-1
- 执行单个case： py.test --alluredir=./test_report testcase/http/kyc/test_kyc_info.py --settings=config.cht-test-1
- 执行单个方法接口： pytest Testcase/Login/test_login.py::TestDeposit::test_get_coin_address --settings=config.cht-test-1

启动allure
allure serve ./test_report
